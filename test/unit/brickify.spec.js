describe('TEST brickify', function(){

    const jQuery = require('jquery');
    const $ = jQuery;
    window.jQuery = jQuery;
    require('../../dist/swite-brickify.min.js')

    it('Should find jquery', function(){
        expect($ === undefined).toBe.false;
    });

    it('Should initialize brickify with 1 column', function () {
        var container = $("<div class='container'></div>").append(
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px")
        );
        $('body').append( container );

        container.brickify();
        expect(container.data('colNum') ).toBe(1);
        // shoud set position relative to all children
        container.children().each(function(){
            expect($(this).css('position')).toBe('relative');
        });
    });


    it('Should initialize brickify from custom configuration', function () {

        var numCol = 4;
        var container = $("<div class='container'></div>").append(
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px"),
            $("<div></div>").css('height', "20px")
        );
        $('body').append(container);

        container.brickify({
            gridSystem: {
                'SM': {
                    'width': 0,
                    'columns': numCol
                }
            }
        });
        expect(container.data('colNum')).toBe(numCol);
        // shoud set position absolute to all children
        container.children().each(function(){
            expect($(this).css('position')).toBe('absolute');
        });

    });


});