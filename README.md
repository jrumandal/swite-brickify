# README #

##Installation
###npm
`npm install swite-brickify.js`

##Usage
Include jquery extension `swite-brickify.js`

`<script src="swite-brickify.js"></script>`

###Basic Initialization

```js
    $(function(){
        $('container').brickify();
    })
```

###Full Initialization

```js
    $(function(){
        $('container').brickify({
            cssTransition: true,
            rebuildOnResize: true,
            gridSystem: {
                'LG': {
                    width: 1200,
                    columns: 4
                },
                'MD': {
                    width: 800,
                    columns: 3
                },
                'SM': {
                    width: 500,
                    columns: 2
                }
            }
        });
    })
```

###Options

Option                  | Description
----------------------- | ---------------
cssTransition           | Enable/Disable CSS transition
rebuildOnResize         | Enable/Disable auto redraw on window resize
gridSystem              | Set custom grid system configuration

#####cssTransition
type:
`bool`

default: 
`false`

#####rebuildOnResize
type:
`bool`

default: 
`false`

#####gridSystem
type:
`object`

default: 
```js
    {
        'LG': {
            width: 1100,
            columns: 4
        },
        'MD': {
            width: 768,
            columns: 3
        },
        'SM': {
            width: 480,
            columns: 2
        }
    }
```

###Reload/Rebuild

```js
    $('container').brickify('reload');
```

###Destroy

```js
    $('container').brickify('destroy');
```


Follow Us [swite.com](https://www.swite.com/)
